from tp2 import *
def test_box_create():
    b = Box()

def test_box_add():
    b=Box()
    b.add('truc1')
    b.add('truc2')

def test_box_in():
    b=Box()
    b.add('truc1')
    b.add('truc2')
    assert('truc1' in b)
    assert('truc2' in b)
    assert('truc3' not in b)

def test_box_remove():
    b=Box()
    b.add('truc1')
    b.add('truc2')
    b.add('toto')
    b.remove('toto')
    assert('truc1' in b)
    assert('toto' not in b)

def test_box_open():
    b=Box()
    b.add('truc1')
    b.add('truc2')
    b.add('toto')
    b.close()
    assert not b.is_open()
    b.open()
    assert b.is_open()

def test_box_action():
    b=Box()
    b.add('truc1')
    b.add('truc2')
    b.add('toto')
    assert b.action_look() == "La boite contient truc1 et truc2 et toto "

def test_thing_create():
    t=Thing(3)

def test_thing_volume():
    t=Thing(10)
    assert t.volume() == 10

def test_box_capacity():
    b=Box()
    b.set_capacity(5)
    assert b.capacity() == 5

def test_box_has_room_for_and_add():
    b=Box()
    b.set_capacity(10)
    t=Thing(3)
    assert b.has_room_for(t)
    b.action_add(t)
    assert t in b._contents
