class Box :
    def __init__(self):
        self._contents=[]
        self._open=True
        self._capacity=None

    def add(self,truc):
        self._contents.append(truc)

    def __contains__(self,machin):
        return machin in self._contents

    def remove(self,machin):
        self._contents.remove(machin)

    def close(self):
        self._open=False

    def open(self):
        self._open=True

    def is_open(self):
        return self._open

    def action_look(self):
        if not self.is_open():
            return "La boite est fermée"
        else:
            res="La boite contient "
            for i in range(len(self._contents)):
                res+=self._contents[i]+" "
                if i<len(self._contents)-1:
                    res+="et "
            return res

    def affiche(self):
        print(self.action_look())

    def set_capacity(self,cap):
        self._capacity=cap

    def capacity(self):
        return self._capacity

    def has_room_for(self,t):
        if t.volume()<=self.capacity():
            return True
        return False


    def action_add(self,t):
        if self.is_open() and self.has_room_for(t):
            self.add(t)
            self.set_capacity(self._capacity-t.volume())
            return True
        else:
            return False




class Thing:
    def __init__(self,v=0):
        self._volume=v
        self._name=None

    def volume(self):
        return self._volume

    def set_name(name):
        self._name=name

    def __repr__(self):
        return self._name

    def has_name(name):
        return self._name==name
